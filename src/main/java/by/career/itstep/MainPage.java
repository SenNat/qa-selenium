package by.career.itstep;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import static by.career.itstep.util.WaitUtil.waitUntilVisible;

// http://13.53.199.26:8080/swagger-ui.html/
// http://13.53.199.26:8080/index
public class MainPage {

    private WebDriver driver;
    private static final String URL = "http://13.48.31.160:8080/index";
    private static final By FIELD_NAME = By.xpath("//input[@id = 'name']");
    private static final By FIELD_LASTNAME = By.xpath("//input[@id = 'surname']");
    private static final By FIELD_EMAIL = By.xpath("//input[@id = 'email']");
    private static final By FIELD_PHONE = By.xpath("//input[@id = 'tel']");
    private static final By CHECKBOX_STUDENT = By.xpath("//label[@for = 'studentStep']");
    private static final By FIELD_GROUP = By.xpath("//input[@id = 'numberGroup']"); // должно открыться доп.поле
    private static final By FIELD_COMM = By.xpath("//input[@id = 'exampleFormControlTextarea1']");

    private static final By SELECT_DAY = By.xpath("//select[@id = 'inputStateDay']"); // это селекты!!
    private static final By SELECT_TIME = By.xpath("//select[@id = 'inputStateTime']");


    public MainPage(WebDriver driver) {
        this.driver = driver;

    }

    public void open() {
        driver.get(URL);
    }

    public boolean atPage() {
        return driver.getCurrentUrl().equals(URL);
    }

    public boolean isColsultationModalOpen() {
        WebElement modal = driver.findElement(By.xpath("//div[@id='modalConsult']"));
        // waitUntilVisible(driver, By.xpath("//div[@id='modalConsult']"), 10);
        // WebElement modal = driver.findElement(By.xpath("//div[@id='modalConsult']"));
        String classes = modal.getAttribute("class");
        return classes.contains("show");
    }
//    public boolean isColsultationModalIs() {
//        WebElement modal = driver.findElement(By.xpath("//div[@id='Submit']"));
//        String classes = modal.getAttribute("class");
//        return classes.contains("btnByBtn");
//    }

    public void openConsultationModal() {
        WebElement link = waitUntilVisible (driver, By.xpath("//ul[@id='myMenu']/li/a[contains(text(),'Консультация')]"), 10);
        link.click();
        waitUntilVisible (driver, By.xpath("//div[@id='modalConsult']"), 10);

    }

    public void fillConsultationForm(String name, String lastName, String email, String phone, boolean isStudent, String group, String comm) throws Exception {
//        WebElement fieldName = driver.findElement(FIELD_NAME);
//        fieldName.sendKeys(name); если мы не планируем больше этот метод использовать, делаем так:
        driver.findElement(FIELD_NAME).sendKeys(name);
        driver.findElement(FIELD_LASTNAME).sendKeys(lastName);
        driver.findElement(FIELD_EMAIL).sendKeys(email);
        driver.findElement(FIELD_PHONE).sendKeys(phone);

        if (isStudent) {
            driver.findElement(CHECKBOX_STUDENT).click();
            Thread.sleep(2000);
            driver.findElement(FIELD_GROUP).sendKeys(group);
        }
        // driver.findElement(FIELD_COMM).sendKeys(comm);

        WebElement elementDay = driver.findElement(SELECT_DAY);
        Select selectDay = new Select(elementDay);
        selectDay.selectByIndex(1);
        Thread.sleep(1000);
        WebElement elementTime = driver.findElement(SELECT_TIME);
        Select selectTime = new Select(elementTime);
        selectTime.selectByIndex(0); // потому что там нет выберите время

        driver.findElement(SELECT_DAY);
        driver.findElement(SELECT_TIME);


    }
}
