package by.career.itstep;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_OK;

public class TestEventAPI {

    private static final String BASE_URL = "http://13.48.31.160:8080/api/v1";
    private static final String STATUS_PUBLISHED = "PUBLISHED";

    @Test
    public void testFindAllEvents_HappyPath() {
        int page = 1;
        int size = 3; // начальные параметры создаются
        //  String email = /// и т.д.

        RestAssured.baseURI = BASE_URL;
        String path = String.format("events?page=%s&size=%s", page, size); // писать так всегда

        RequestSpecification req = RestAssured.given(); //req можно кому-то отправить
        Response response = req.request(Method.GET, path);// делвем реквест методом гет по этому пути и это приклеится к указанному выше урлу (BASE_URL)
        String jsonBody = response.getBody().asString(); //достаем все что нам надо для теста - боди и код ответа:
        // String value = response.getBody().jsonPath().get("");// указать ключ (ID, main... и т.д.)
        int code = response.getStatusCode();

        System.out.println("Body: " + jsonBody);
        System.out.println("Code: " + code);

        Assert.assertEquals(SC_OK, code); // и тут их много может быть разных, этих асертов

    }

    @Test
    public void testFindEventByID_HappyPath() {
        String id = "1469a0b5-6640-4184-a136-05a09aa9d719"; // сюда ID вставить, достав из боди. Сейчас возьмем из свагера
        RestAssured.baseURI = BASE_URL;
        String path = String.format("events/%s", id);

        RequestSpecification req = RestAssured.given(); //req можно кому-то отправить
        Response response = req.request(Method.GET, path);// делвем реквест методом гет по этому пути (который мы уже изменили по
        // сравнению с предыдущим тестом) и это приклеится к указанному выше урлу (BASE_URL)
        String jsonBody = response.getBody().asString(); //достаем все что нам надо для теста - боди и код ответа:
        // String value = response.getBody().jsonPath().get("");// указать ключ (ID, main... и т.д.)
        int code = response.getStatusCode();
        String status = response.getBody().jsonPath().get("status"); // чтобы извлечь информацию о статусе


        System.out.println("Body: " + jsonBody);
        System.out.println("Code: " + code);
        System.out.println("Status: " + status);

        Assert.assertEquals(code, SC_OK); // и тут их много может быть разных, этих асертов
        Assert.assertEquals(status, STATUS_PUBLISHED);

    }

    // поиск по неправильному URL:
    @Test
    public void testFindEventByIDIFNotExist() {
        String wrongId = UUID.randomUUID().toString(); // неверный ID, которого не существует
        RestAssured.baseURI = BASE_URL;
        String path = String.format("events/%s", wrongId);

        RequestSpecification req = RestAssured.given(); //req можно кому-то отправить
        Response response = req.request(Method.GET, path);// делвем реквест методом гет по этому пути (который мы уже изменили по
        // сравнению с предыдущим тестом) и это приклеится к указанному выше урлу (BASE_URL)
        String jsonBody = response.getBody().asString(); //достаем все что нам надо для теста - боди и код ответа:
        // String value = response.getBody().jsonPath().get("");// указать ключ (ID, main... и т.д.)
        int code = response.getStatusCode();

        System.out.println("Body: " + jsonBody);
        System.out.println("Code: " + code);


        Assert.assertEquals(code, SC_NOT_FOUND); // соответствует 404

    }

}
