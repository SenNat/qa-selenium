package by.career.itstep;

import by.career.itstep.MainPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;


public class TestMainPage {


    WebDriver driver;
    MainPage page;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        page = new MainPage(driver);
    }

    @Test
    public void test() {

        page.open();
        assertFalse(page.isColsultationModalOpen());
//        assertFalse(page.isColsultationModalIs());

        assertTrue(page.atPage());
//        driver.close();
    }

    @Test
    public void testConsultationModal() throws Exception {
        page.open();
        page.openConsultationModal();
        Thread.sleep(3000); //ждем 1 сек
        assertTrue(page.isColsultationModalOpen());
    }


    @Test
    public void testFillConsultationModal_happyPath() throws Exception {
        page.open();
        page.openConsultationModal();
        // Thread.sleep(3000); заменяем на
        assertTrue(page.isColsultationModalOpen());
        page.fillConsultationForm("Alex", "Smith", "aaa@aaa.com", "+45687777", true, "QA3819", "asddfdgslgkjdlskfg");
        // Thread.sleep(10000);
    }

    @AfterClass
    public void shutDown() {
        driver.close();
    }
}

