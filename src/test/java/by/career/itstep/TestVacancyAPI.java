package by.career.itstep;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_CREATED;


public class TestVacancyAPI {
    //[ INTERN, JUNIOR, MIDDLE, SENIOR ]
    private static final String EXP_INTERN = "INTERN";
    private static final String EXP_JUNIOR = "JUNIOR";
    private static final String EXP_MIDDLE = "MIDDLE";
    private static final String EXP_SENIOR = "SENIOR";

    private static final String BASE_URL = "http://13.48.31.160:8080/api/v1";

    @Test
    public void testCreateVacancy_happyPath() {
        RestAssured.baseURI = BASE_URL;
        String path = "/vacancies";
        //просим вернуть req:
        RequestSpecification req = RestAssured.given();

        //положить сообщение в боди:
        JSONObject json = new JSONObject();
        //"company": "string",
        //  "description": "string",
        //  "experience": "INTERN",
        //  "name": "string"
        json.put("company", "IT-step");
        json.put("description", "Cool job description");
        json.put("experience", EXP_JUNIOR);
        json.put("name", "CoolDeveloper");
        // соединить реквест и json:
        //используем хедеры для этого:

        req.header("Content-Type", "application/json");
        //положим json-строку в боди:
        req.body(json.toJSONString());

        Response response = req.request(Method.POST, path);
        String body = response.getBody().asString();
        int code = response.getStatusCode();

        System.out.println("Body: " + body);
        System.out.println("Code: " + code);

        Assert.assertEquals(code, SC_CREATED); // соотв. 201

    }

    // проверим создание вакансии, если она невалидная:
    @Test
    public void testCreateVacancyIfNotValid() {
        RestAssured.baseURI = BASE_URL;
        String path = "/vacancies";
        //просим вернуть req:
        RequestSpecification req = RestAssured.given();

        //положить сообщение в боди:
        JSONObject json = new JSONObject();
        //"company": "string",
        //  "description": "string",
        //  "experience": "INTERN",
        //  "name": "string"
        json.put("company", ""); // невалидное имя
        json.put("description", "Cool job description");
        json.put("experience", EXP_JUNIOR);
        json.put("name", "CoolDeveloper");
        // соединить реквест и json:
        //используем хедеры для этого:

        req.header("Content-Type", "application/json");
        //положим json-строку в боди:
        req.body(json.toJSONString());

        Response response = req.request(Method.POST, path);
        String body = response.getBody().asString();
        int code = response.getStatusCode();

        System.out.println("Body: " + body);
        System.out.println("Code: " + code);

        Assert.assertEquals(code, SC_BAD_REQUEST); // соотв. 404
    }

    @Test
    public void testFindVacancyByID_happyPath() {
        String id = "e886224f-370f-4ab7-969b-a653cc525535"; // не очень хорошо, потмоу что ее могут удалить, эту вакансию
        Response response = RestAssured.given()
                .when()
                .get("")
                .getBody()
                .jsonPath ().get("key"); // вызываем методы другими методами

    }

}
